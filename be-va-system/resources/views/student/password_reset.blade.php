<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LNU Voting System - Password Reset</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&display=swap" rel="stylesheet">
        
         <style>
             * {
                 box-sizing: border-box;
                 margin: 0;
                 padding: 0;
             }

             body {
              
                 background: #fff;
                 font-family: 'Poppins', sans-serif;
            }
            main {
                width: 100%;
                 height: 100vh;
                 display: flex;
                 flex-direction: column;
                 justify-content: center;
                 align-items: center;
                 background: url({{ asset('misc/lnu.jpg')}}), #FED136;
                 background-position: center;
                 background-repeat: no-repeat;
                 background-blend-mode: multiply;
                 background-size: cover;
            }

            .email-container {
                width: 500px;
                height: 700px;
                padding: 20px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
                background: #fff;
            }

            .email-img img {
                width: 250px;
               height: 250px;
               /* margin: 20px 0; */
            }

            .email-info {
                margin-top: 20px;
                /* padding: 0 50px; */
            }

            .btn-password-reset {
                background: #070372;
                color: #fff;
            }

            .btn-password-reset:hover {
                color: #070372;
                background: #fff;
                border: 1px #070372 solid;
            }

            input {
                width: 20rem !important;
            }
         </style>
</head>
<body>
    <main>
        <div class="email-container shadow">
            <div class="email-img">
                <img src="{{ asset('misc/forgot-password.svg') }}" alt="qwewqe">
            </div>
            <h3>Student Reset Password</h3>
            <p>Please input your new password to continue</p>
            <div class="email-info">
                <form action="{{ url("/reset-password/$email") }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-floating mb-3">
                        <input class="form-control" type="password" name="password" id="password" placeholder="New Password">
                        <label for="password">New Password</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm New Password">
                        <label for="password_confirmation">Confirm New Password</label>
                    </div>
                    <button class="btn btn-password-reset" type="submit"><i class="bi bi-key mx-1"></i>Reset Password</button>
                </form>
                
            </div>
        </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

