<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $content['title'] }}</title>


         <!-- Bootstrap core CSS -->
         <link href = {{ asset("bootstrap/css/bootstrap.css") }} rel="stylesheet" />

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&display=swap" rel="stylesheet">

         <style>
             * {
                 box-sizing: border-box;
                 margin: 0;
                 padding: 0;
             }

             body {
              
                 background: #fff;
                 font-family: 'Poppins', sans-serif;
            }
            main {
                width: 100%;
                 height: 100vh;
                 display: flex;
                 flex-direction: column;
                 justify-content: center;
                 align-items: center;
                 text-align: start;
                 padding: 0 20px;
            }
         </style>
</head>
<body>
    <main>
        <h1 class="mb-5">{{ $content['title'] }}</h1>
        <p>
            {!! $content['content'] !!}
        </p>
        <p> - {{ $content['posted_by']}} @ {{ \Carbon\Carbon::parse($content['posted_at'])->format('d-m-Y')}} ({{ $content['organization'] }})</p>
    </main>
</body>
</html>