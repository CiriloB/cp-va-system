<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LNU Voting System - Email Verification Request</title>

         <!-- Bootstrap core CSS -->
         <link href = {{ asset("bootstrap/css/bootstrap.css") }} rel="stylesheet" />

         <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&display=swap" rel="stylesheet">

         <style>
             * {
                 box-sizing: border-box;
                 margin: 0;
                 padding: 0;
             }

             body {
              
                 background: #fff;
                 font-family: 'Poppins', sans-serif;
            }
            main {
                width: 100%;
                 height: 100vh;
                 display: flex;
                 flex-direction: column;
                 justify-content: center;
                 align-items: center;
                 background: url({{ asset('misc/lnu.jpg')}}), #FED136;
                 background-position: center;
                 background-repeat: no-repeat;
                 background-blend-mode: multiply;
                 background-size: cover;
            }

            .email-container {
                width: 500px;
                padding: 20px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
                background: #fff;
            }

            .email-img img {
                width: 250px;
               height: 250px;
               margin: 20px 0;
            }

            .email-info {
                text-align: center;
                margin-top: 20px;
                padding: 0 50px;
            }

            .email-info h3,
            .email-info h5 {
                margin: 20px 0;
            }

            .btn-confirm-email {
                background: #070372;
                color: #fff;
            }

            .btn-confirm-email:hover {
                color: #070372;
                background: #fff;
                border: 1px #070372 solid;

            }
         </style>
</head>
<body>
    <main>
        <div class="email-container shadow">
            <div class="email-img">
                <img src="{{ asset('misc/cancel.svg') }}" alt="qwewqe">
            </div>
            <div class="email-info">

                <h3>
                    Email Verification Failed
                </h3>
                <h5>
                    Please resend your email verification request
                </h5>
                <div style="text-align: start; margin: 50px 0;">
                    <p>
                        All the best,
                        <br>
                        The LNU Voting System team
                    </p>
                </div>
                
            </div>
        </div>
    </main>
</body>
</html>