<?php

namespace Tests\Unit;

use App\Models\Student;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class StudentTest extends TestCase
{

    use WithFaker;

    public function test_api_can_login_student() {
        // Kuhaon an url han authentication route han login
        $baseUrl = Config::get('app.url') . '/api/auth/student/login';
        // Sample input for student number and password
        $student_number = Config::get('app.student_number');
        $student_password = Config::get('app.student_password');

        // Gin test an route base han base url tpos gin pass an mga values
        $response = $this->json('POST', $baseUrl . '/', [
            'student_number' => $student_number,
            'password' => $student_password
        ]);

        // Amo inen an expected na status code kun successfull an request
        $response
            ->assertStatus(200);
            
    }

    public function test_api_can_login_with_pending_status_of_student() {
        $baseUrl = Config::get('app.url') . '/api/auth/student/login';
        $student_number = Config::get('app.student_number');
        $student_password = Config::get('app.student_password');
     
        $response = $this->json('POST', $baseUrl . '/', [
            'student_number' => $student_number,
            'password' => $student_password
        ]);
    
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'student_status'
            ]);
    }

    public function test_api_can_logged_out_student() {
        $user = Student::where('student_number', Config::get('app.student_number'))->first();
        $token = JWTAuth::fromUser($user);
        $baseUrl = Config::get('app.url') . '/api/auth/student/logout?token=' . $token;

        $response = $this->json('POST', $baseUrl, []);

        $response
            ->assertStatus(200);
    }

    public function test_api_can_get_current_logged_in_student()
    {
        $user = Student::where('student_number', Config::get('app.student_number'))->first();
        $token = JWTAuth::fromUser($user);
        $baseUrl = Config::get('app.url') . '/api/auth/student/me?token=' . $token;
        $response = $this->json('POST', $baseUrl . '/', []);
        $response->assertStatus(200);
    }
}