<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentSchoolInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_school_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId("college_id")->constrained();
            $table->foreignId("course_id")->constrained();
            $table->foreignId("semester_id")->constrained();
            $table->foreignId("year_level_id")->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_school_infos');
    }
}
