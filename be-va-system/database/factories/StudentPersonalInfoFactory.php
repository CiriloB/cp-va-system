<?php

namespace Database\Factories;

use App\Models\StudentPersonalInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentPersonalInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StudentPersonalInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'last_name' =>  $this->faker->lastName,
            'gender' => 'MALE',
            'mobile_number' => $this->faker->phoneNumber,
        ];
    }
}
