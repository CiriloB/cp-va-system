<?php

namespace Database\Factories;

use App\Models\StudentSchoolInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentSchoolInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StudentSchoolInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'college_id' => 2,
            'course_id' => 14,
            'school_year_id' => 1,
            'year_level_id' => 3,
            'semester_id' => 2,
        ];
    }
}
