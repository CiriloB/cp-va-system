<?php

namespace Database\Factories;

use App\Models\StudentAccount;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class StudentAccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StudentAccount::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'student_number' => rand(1700000, 1900000),
            'email' => $this->faker->email,
            'password' => Hash::make('123123'),
            'status' => 'APPROVED',
        ];
    }
}
