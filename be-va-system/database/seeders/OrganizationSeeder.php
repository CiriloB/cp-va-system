<?php

namespace Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $organizations = [
            // SSC
            ['organization' => 'Student Supreme Council', 'abbreviation' => 'SSC'],
            // Year Level
            ['organization' => 'Freshmen Class Organization', 'abbreviation' => 'FCO'],
            ['organization' => 'Sophomore Class Organization', 'abbreviation' => 'SCO'],
            ['organization' => 'Junior Class Organization', 'abbreviation' => 'JCO'],
            ['organization' => 'Senior Class Organization', 'abbreviation' => 'SenCo'],
            // College
            ['organization' => 'College of Education', 'abbreviation' => 'COE'],
            ['organization' => 'College of Arts and Sciences', 'abbreviation' => 'CAS'],
            ['organization' => 'College of Management and Entrepreneurship', 'abbreviation' => 'CME'],
            // Student
            ['organization' => 'Association of Political Science Students', 'abbreviation' => 'APSS'], //10
            ['organization' => 'Association of Values Educators', 'abbreviation' => 'AVED'],
            ['organization' => 'BACommUNITY'],
            ['organization' => 'Book Enthusiasts'],
            ['organization' => 'BPED Movers'],
            ['organization' => 'Circle of Future Educators', 'abbreviation' => 'CofEd'],
            ['organization' => 'Developmental Integrated Group of Information Technology Students', 'abbreviation' => 'DIGITS'],
            ['organization' => 'Early Childhood Educators Organization', 'abbreviation' => 'ECEO'],
            ['organization' => 'English Circle'],
            ['organization' => 'Entrepreneurs Club'],
            ['organization' => 'Hoteliers’ and Restaurateurs’ Circle', 'abbreviation' => 'HRC'],
            ['organization' => 'Interact Society'],
            ['organization' => 'Junior Social Workers\' Association of Philippines - LNU Chapter', 'abbreviation' => 'JSWAP'],
            ['organization' => 'Kapisanang Maka-Filipino', 'abbreviation' => 'KMF'],
            ['organization' => 'Math Student\'s Society', 'abbreviation' => 'MSS'],
            ['organization' => 'Science Questers Unlimited', 'abbreviation' => 'SQU'],
            ['organization' => 'Special Educators League', 'abbreviation' => 'SpEL'],
            ['organization' => 'Tourism Circle', 'abbreviation' => 'TC'],
            ['organization' => 'Technology and Livelihood Educators Guild', 'abbreviation' => 'TLE Guild'],
            ['organization' => 'Association of Student Tour Guides', 'abbreviation' => 'ASTG'], //29
        ];

        foreach($organizations as $organization) {
            Organization::create($organization);
        }

    }
}
