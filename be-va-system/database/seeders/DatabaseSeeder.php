<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            OrganizationSeeder::class,
            PartySeeder::class,
            CollegeSeeder::class,
            CourseSeeder::class,
            YearLevelSeeder::class,
            SchoolYearSeeder::class,
            SemesterSeeder::class,
            PositionSeeder::class,
            AccessLevelSeeder::class,
            AdminSeeder::class,
            StudentSeeder::class,
            ElectionTypeSeeder::class
        ]);
    }
}
