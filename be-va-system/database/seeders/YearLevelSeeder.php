<?php

namespace Database\Seeders;

use App\Models\YearLevel;
use Illuminate\Database\Seeder;

class YearLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $year_levels = [
            ['year_level' => '1st Year', 'organization_id' => 3],
            ['year_level' => '2nd Year', 'organization_id' => 4],
            ['year_level' => '3rd Year', 'organization_id' => 5],
            ['year_level' => '4th Year', 'organization_id' => 6]
        ];

        foreach($year_levels as $year_level) {
            YearLevel::create($year_level);
        }
    }
}
