<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $positions = [
            ['name' => 'President'],
            ['name' => 'Vice President'],
            ['name' => 'Secretary'],
            ['name' => 'Auditor'],
            ['name' => 'Treasurer'],
            ['name' => 'P.I.O'],
        ];

        foreach($positions as $position) {
            Position::create($position);
        }
    }
}
