<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = [
            // COE
            ['course' => 'Bachelor of Elementary Education', 'abbreviation' => 'BEED', 'college_id' => 1, 'organization_id' => 15],
            ['course' => 'Bachelor of Early Child Education', 'abbreviation' => 'BECED', 'college_id' => 1, 'organization_id' => 17],
            ['course' => 'Bachelor of Special Needs Education', 'abbreviation' => 'BSNED', 'college_id' => 1, 'organization_id' => 26],
            ['course' => 'Bachelor of Technology and Livelihood Education', 'abbreviation' => 'BTELD', 'college_id' => 1, 'organization_id' => 28],
            ['course' => 'Bachelor of Physical Education', 'abbreviation' => 'BPED' , 'college_id' => 1, 'organization_id' => 14],
            ['course' => 'Bachelor of Secondary Education Major in English', 'abbreviation' => 'BSED - English', 'college_id' => 1, 'organization_id' => 18],
            ['course' => 'Bachelor of Secondary Education Major in Filipino', 'abbreviation' => 'BSED - Filipino', 'college_id' => 1, 'organization_id' => 23],
            ['course' => 'Bachelor of Secondary Education Major in Mathematics', 'abbreviation' => 'BSED - Mathematics', 'college_id' => 1, 'organization_id' => 24],
            ['course' => 'Bachelor of Secondary Education Major in Science', 'abbreviation' => 'BSED - Science', 'college_id' => 1, 'organization_id' => 25],
            ['course' => 'Bachelor of Secondary Education Major in Social Studies', 'abbreviation' => 'BSED - Social Studies', 'college_id' => 1, 'organization_id' => 21],
            ['course' => 'Bachelor of Secondary Education Major in Values Education', 'abbreviation' => 'BSED - Values Education', 'college_id' => 1, 'organization_id' => 11], 
            // CASs
            ['course' => 'Bachelor of Arts in Communication', 'abbreviation' => 'BA Comm', 'college_id' => 2, 'organization_id' => 12],
            ['course' => 'Bachelor of Library and Information Science', 'abbreviation' => 'BLIS', 'college_id' => 2, 'organization_id' => 13],
            ['course' => 'Bachelor of Science in Information Technology', 'abbreviation' => 'BSIT' , 'college_id' => 2, 'organization_id' => 16],
            // ['course' => 'Bachelor of Arts in English Language', 'abbreviation' => 'BAEL', 'college_id' => 2, 'organization_id' => 3],
            ['course' => 'Bachelor of in Political Science', 'abbreviation' => 'BAPoS', 'college_id' => 2, 'organization_id' => 10],
            ['course' => 'Bachelor of in Science in Biology', 'abbreviation' => 'BSBio', 'college_id' => 2, 'organization_id' => 25],
            ['course' => 'Bachelor of in Science in Social Work', 'abbreviation' => 'BSSW', 'college_id' => 2, 'organization_id' => 22],
            // CME
            ['course' => 'Bachelor of in Science in Tourism Management', 'abbreviation' => 'BSTM', 'college_id' => 3, 'organization_id' => 27],
            ['course' => 'Bachelor of in Science in Hospitality Management', 'abbreviation' => 'BSHM', 'college_id' => 3, 'organization_id' => 20],
            ['course' => 'Bachelor of in Science in Entrepreneurship', 'abbreviation' => 'BSEntrep', 'college_id' => 3, 'organization_id' => 19],
        ];
        //

        foreach($courses as $course) {
            Course::create($course);
        }
    }
}
