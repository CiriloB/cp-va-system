<?php

namespace Database\Seeders;

use App\Models\AccessLevel;
use Illuminate\Database\Seeder;

class AccessLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $access_levels = [
            ['access_level' => 'Admin'],
            ['access_level' => 'Organization'],
            ['access_level' => 'Teacher'],
        ];

        foreach($access_levels as $access_level) {
            AccessLevel::create($access_level);
        }
    }
}
