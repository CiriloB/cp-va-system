<?php

namespace Database\Seeders;

use App\Models\Party;
use Illuminate\Database\Seeder;

class PartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $parties = [
            ['party' => 'Liberal', 'description' => 'Party Description'],
            ['party' => 'Lakas', 'description' => 'Party Description'],
            ['party' => 'PDP', 'description' => 'Party Description'],
            ['party' => 'Kape', 'description' => 'Party Description'],
        ];

        foreach($parties as $party) {
            Party::create($party);
        }
    }
}
