<?php

namespace Database\Seeders;

use App\Models\ElectionType;
use Illuminate\Database\Seeder;

class ElectionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $election_types = [
            ['name' => 'SSC'],
            ['name' => 'College Organization'],
            ['name' => 'Year Level Organization'],
            ['name' => 'Student Organization'],
        ];

        foreach($election_types as $election_type) {
            ElectionType::create($election_type);
        }
    }
}
