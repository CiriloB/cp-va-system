<?php

namespace Database\Seeders;

use App\Models\Announcement;
use Illuminate\Database\Seeder;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $announcements = [
            [
                'title' => 'SSC Election', 
                'description' => 'This is an SSC election', 
                'content' => '<p>sample</p><p>text</p>', 
                'slug' => 'ssc-election', 
                'organization_id' => 1
            ],
            [
                'title' => 'SSC Election - Know the Candidates', 
                'description' => 'This is an SSC election candidates', 
                'content' => '<p>sample</p><p>text</p>', 
                'slug' => 'ssc-election', 
                'organization_id' => 1
            ],
            [
                'title' => 'SSC Election', 
                'description' => 'This is an SSC election', 
                'content' => '<p>sample</p><p>text</p>', 
                'slug' => 'ssc-election', 
                'organization_id' => 1
            ],
        ];

        foreach($announcements as $announcement) {
            Announcement::create($announcement);
        }
    }
}
