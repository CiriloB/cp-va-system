<?php

namespace Database\Seeders;

use App\Models\SchoolYear;
use Illuminate\Database\Seeder;

class SchoolYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $school_years = [
            ['start_year' => '2021', 'end_year' => '2022']
        ];

        foreach($school_years as $school_year) {
            SchoolYear::create($school_year);
        }
    }
}
