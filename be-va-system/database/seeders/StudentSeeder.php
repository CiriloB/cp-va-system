<?php

namespace Database\Seeders;

use App\Models\College;
use App\Models\Course;
use App\Models\OrganizationFilter;
use App\Models\Student;
use App\Models\StudentAccount;
use App\Models\StudentPersonalInfo;
use App\Models\StudentSchoolInfo;
use App\Models\YearLevel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student_account = StudentAccount::create([
            'student_number' => '1800760',
            'email' => '1800760@lnu.edu.ph',
            'password' => Hash::make('1800760'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Cirilo',
            'middle_name' => 'Espinisin',
            'last_name' => 'Bucatcat',        
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);


        $student_account = StudentAccount::create([
            'student_number' => '1800769',
            'email' => '1800769@lnu.edu.ph',
            'password' => Hash::make('1800769'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Kathleen',
            'middle_name' => 'Alapoop',
            'last_name' => 'Igarta',
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);


        $student_account = StudentAccount::create([
            'student_number' => '1800757',
            'email' => '1800757@lnu.edu.ph',
            'password' => Hash::make('1800757'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Wallin',
            'middle_name' => 'Alfredo',
            'last_name' => 'Badidles',
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1800932',
            'email' => '1800932@lnu.edu.ph',
            'password' => Hash::make('1800932'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Arjehn Paul',
            'middle_name' => 'Miranda',
            'last_name' => 'Collantes',   
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);
        
        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1800647',
            'email' => '1800647@lnu.edu.ph',
            'password' => Hash::make('1800647'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Derick Justin',
            'middle_name' => 'Moral',
            'last_name' => 'Durante',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1802998',
            'email' => '1802998@lnu.edu.ph',
            'password' => Hash::make('1802998'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'John Lloyd',
            'middle_name' => ' ',
            'last_name' => 'Rosanes',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id

        ]);


        
        $student_account = StudentAccount::create([
            'student_number' => '1803019',
            'email' => '1803019@lnu.edu.ph',
            'password' => Hash::make('1803019'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Lowell',
            'middle_name' => 'Simbahon',
            'last_name' => 'Tebrero',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);


        $student_account = StudentAccount::create([
            'student_number' => '1802946 ',
            'email' => '1802946@lnu.edu.ph',
            'password' => Hash::make('1802946 '),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Ana Patriz',
            'middle_name' => 'Negros',
            'last_name' => 'Bareja',
            
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1800866',
            'email' => '1800866@lnu.edu.ph',
            'password' => Hash::make('1800866'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Roniel Fernan',
            'middle_name' => '',
            'last_name' => 'Larion',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);


        $student_account = StudentAccount::create([
            'student_number' => '1800642',
            'email' => '1800642@lnu.edu.ph',
            'password' => Hash::make('1800642'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Ian',
            'middle_name' => 'Peñaranda',
            'last_name' => 'Dacillo',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);


        $student_account = StudentAccount::create([
            'student_number' => '1800708',
            'email' => '1800708@lnu.edu.ph',
            'password' => Hash::make('1800708'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Melbienri',
            'middle_name' => '',
            'last_name' => 'Gabitan',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1402017',
            'email' => '1402017@lnu.edu.ph',
            'password' => Hash::make('1402017'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Danica',
            'middle_name' => 'Fuentes',
            'last_name' => 'Barrientos',           
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);

        $student_account = StudentAccount::create([
            'student_number' => '1800652',
            'email' => '1800652@lnu.edu.ph',
            'password' => Hash::make('1800652'),
            'status' => 'APPROVED',
        ]);

        $student_personal_info = StudentPersonalInfo::create([
            'first_name' => 'Genreve',
            'middle_name' => 'Peque',
            'last_name' => 'Fernandez',
            
        ]);

        $student_school_info = StudentSchoolInfo::create([
            'college_id' => 2,
            'course_id' => 16,
            'semester_id' => 2,
            'year_level_id' => 3
        ]);

        $organization_filter = OrganizationFilter::create([
            'year_level' => YearLevel::where('id', 3)->first()->organization_id,
            'college' => College::where('id', 2)->first()->organization_id,
            'course' => Course::where('id', 14)->first()->organization_id
        ]);

        Student::create([
            'student_account_id' => $student_account->id,
            'student_personal_info_id' => $student_personal_info->id,
            'student_school_info_id' => $student_school_info->id,
            'organization_filter_id' => $organization_filter->id
        ]);
    }
}
