<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admins = [

            [
                'first_name' => 'Wallin', 
                'middle_name' => 'Alfredo', 
                'last_name' => 'Badidles', 
                'email' => 'misadmin1@gmail.com', 
                'username' => 'misadmin1', 
                'password' => Hash::make("misadmin1"), 
                'is_active' => 1,
                'access_level_id' => 1],
            [
                'first_name' => 'Kathleen', 
                'middle_name' => 'Alapoop', 
                'last_name' => 'Igarta',
                'email' => 'misadmin2@gmail.com', 
                'username' => 'misadmin2', 
                'password' => Hash::make("misadmin2"), 
                'is_active' => 1,
                'access_level_id' => 1
            ],
            [
                'first_name' => 'Cirilo', 
                'middle_name' => 'Espinisin', 
                'last_name' => 'Bucatcat', 
                'username' => 'bucatcatc', 
                'password' => Hash::make("bucatcatc"), 
                'email' => 'bucatcatc@gmail.com',
                'access_level_id' => 2,
                'role_id' => 2,
                'organization_id' => 1,
                'is_active' => 1,
            ],
            [
                'first_name' => 'Cirilo', 
                'middle_name' => 'Espinisin', 
                'last_name' => 'Bucatcat', 
                'username' => 'bucatcatc', 
                'password' => Hash::make("bucatcatc"), 
                'email' => 'bucatcatc@gmail.com',
                'access_level_id' => 2,
                'role_id' => 1,
                'organization_id' => 16,
                'is_active' => 1,
            ],
            [
                'first_name' => 'Arjehn Paul', 
                'middle_name' => 'Miranda', 
                'last_name' => 'Collantes', 
                'username' => 'apcollantes', 
                'password' => Hash::make("apcollantes"), 
                'email' => 'apcollantes@gmail.com',
                'access_level_id' => 2,
                'role_id' => 2,
                'organization_id' => 16,
                'is_active' => 1,
            ],
          
        ];

        foreach($admins as $admin) {
            Admin::create($admin);
        }
    }
}
