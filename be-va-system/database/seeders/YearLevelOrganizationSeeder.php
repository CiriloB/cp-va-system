<?php

namespace Database\Seeders;

use App\Models\YearLevelOrganization;
use Illuminate\Database\Seeder;

class YearLevelOrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $organizations = [
            ['name' => 'FCO'],
            ['name' => 'SCO'],
            ['name' => 'JCO'],
            ['name' => 'SenCo'],
        ];

        foreach($organizations as $organization) {
            YearLevelOrganization::create($organization);
        }
    }
}
