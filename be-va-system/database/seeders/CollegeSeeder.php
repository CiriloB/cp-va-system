<?php

namespace Database\Seeders;

use App\Models\College;
use Illuminate\Database\Seeder;

class CollegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $colleges = [
            ['college' => 'College of Education', 'organization_id' => 7],
            ['college' => 'College of Arts and Science', 'organization_id' => 8],
            ['college' => 'College of Management and Entrepreneurship', 'organization_id' => 9],
        ];

        foreach($colleges as $college) {
            College::create($college);
        }
    }
}
