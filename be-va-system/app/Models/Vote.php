<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function candidates() {
        return $this->hasMany(Candidate::class,  'id', 'candidate_id');
    }

    public function info(){
        return $this->hasMany(VoteInfo::class, 'vote_id', 'id');
    }

    public function positions() {
        return $this->hasMany(Position::class, 'id', 'position_id');
    }
}
