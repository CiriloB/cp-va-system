<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'username',
        'password',
        'email',
        'is_active',
        'access_level_id',
        'role_id',
        'organization_id',
    ];

    protected $hidden = [
        'password'
    ];
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    public function getJWTCustomClaims() {
          return [];
    }

    public function access_level() {
        return $this->hasOne(AccessLevel::class, 'id', 'access_level_id');
    }

    public function organization() {
        return $this->hasOne(Organization::class, 'id', 'organization_id');
    }

    public function role() {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
}
