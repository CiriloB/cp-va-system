<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function position() {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    public function election() {
        return $this->hasOne(Election::class, 'id', 'election_id');
    }

    public function student() {
        return $this->hasOne(Student::class, 'id', 'student_id');
    }

    public function votes() {
        return $this->hasMany(Vote::class, 'id');
    }

    public function vote_infos() {
        return $this->hasMany(VoteInfo::class, 'candidate_id');
    }
}
