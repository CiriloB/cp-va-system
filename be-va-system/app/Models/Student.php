<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    public function student_account() {
        return $this->hasOne(StudentAccount::class, 'id', 'student_account_id');

    }

    public function student_personal_info() {
        return $this->hasOne(StudentPersonalInfo::class, 'id', 'student_personal_info_id');

    }

    public function student_school_info() {
        return $this->hasOne(StudentSchoolInfo::class, 'id', 'student_school_info_id');
    }

    public function organization_filter() {
        return $this->hasOne(OrganizationFilter::class, 'id', 'organization_filter_id');
    }
   
}
