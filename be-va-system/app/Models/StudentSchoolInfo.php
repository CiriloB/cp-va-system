<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentSchoolInfo extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function college() {
        return $this->hasOne(College::class, 'id', 'college_id');
    }

    public function course() {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function school_year() {
        return $this->hasOne(SchoolYear::class, 'id', 'school_year_id');
    }

    public function year_level() {
        return $this->hasOne(YearLevel::class, 'id', 'year_level_id');
    }
}
