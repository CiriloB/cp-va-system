<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    use HasFactory;

    protected $filllable = [
        'start_year',
        'end_year'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function student_school_info() {
        return $this->belongsTo(StudentSchoolInfo::class);
    }
}
