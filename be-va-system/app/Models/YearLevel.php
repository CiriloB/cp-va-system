<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YearLevel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function student_school_info() {
        return $this->belongsTo(StudentSchoolInfo::class);
    }

    public function organization() {
        return $this->hasOne(YearLevelOrganization::class, 'id', 'year_level_organization_id');
    }
}
