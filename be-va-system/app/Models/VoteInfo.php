<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoteInfo extends Model
{
    use HasFactory;


    protected $fillable = [
        'vote_id',
        'candidate_id',
        'election_id'
    ];

    public function candidate() {
        return $this->belongsTo(Candidate::class, 'candidate_id', 'id');
    }

    public function candidates() {
        return $this->belongsToMany(Candidate::class);
    }
}
