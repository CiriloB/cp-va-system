<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class StudentAccount extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'student_number',
        'email',
        'password',
        'status'
    ];

    protected $hidden = [
        'password'
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
   }

   public function getJWTCustomClaims() {
         return [];
   }  

   public function student() {
       return $this->belongsTo(Student::class, 'id');
   }
}
