<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $filllable = [
        'name',
        'abbreviation'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];
    
    public function student_school_info() {
        return $this->belongsTo(StudentSchoolInfo::class);
    }
    
    public function organization() {
        return $this->hasOne(CourseOrganization::class, 'id', 'course_organization_id');
    }
}
