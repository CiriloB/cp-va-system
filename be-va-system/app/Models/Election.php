<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function candidates(){
        return $this->hasMany(Candidate::class, 'election_id', 'id');
    }

    public function votes(){
        return $this->hasMany(Vote::class, 'election_id', 'id');
    }

    public function candidate_votes() {
        return $this->hasManyThrough(Candidate::class, VoteInfo::class, 'election_id', 'id', 'id', 'candidate_id');
    }
}
