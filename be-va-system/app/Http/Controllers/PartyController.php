<?php

namespace App\Http\Controllers;

use App\Models\Party;
use Illuminate\Http\Request;

class PartyController extends Controller
{
    //

    public function index() {
        return response()->json(Party::all());
    }
}
