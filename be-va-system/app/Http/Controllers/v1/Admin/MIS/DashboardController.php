<?php

namespace App\Http\Controllers\v1\Admin\MIS;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Election;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function ongoingElectionCount() {
        return response()->json(Election::where('status', 'ONGOING')->count());
    }

    public function organizationAccountCount() {
        return response()->json(Admin::where('organization_id', '!=',  null)->where('organization_id', '!=',  null)->count());
    }

    public function organizationCount() {
        return response()->json(Organization::count());
    }

    public function studentAccountCount() {
        return response()->json(Student::count());
        
    }

    public function getOrganizations() {
        return response()->json(Organization::all());
    }

    public function getRoles() {
        return response()->json(Role::all());
    }
}
