<?php

namespace App\Http\Controllers\v1\Admin\MIS\UserAccounts;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class OrganizationAccountController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {
        return response()->json(Admin::where('access_level_id', '!=', 1)->with(['organization', 'role'])->latest()->get());
    }

    public function store(Request $request) {

        $this->validate($request, [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed',
        ]);

        Admin::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'is_active' => 1,
            'access_level_id' => 2,
            'role_id' => $request->role_id,
            'organization_id' => $request->organization_id
        ]);

    }

    public function update(Request $request) {

        $this->validate($request, [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'username' => 'required',
        ]);

        $admin = Admin::where('id', $request->id)->first();
        $admin->first_name = $request->first_name;
        $admin->middle_name = $request->middle_name;
        $admin->last_name = $request->last_name;
        $admin->email = $request->email;
        $admin->username = $request->username;
        $admin->role_id = $request->role_id;
        $admin->organization_id = $request->organization_id;
        $admin->save();

    }

    public function destroy($id) {
        Admin::destroy($id);
        
    }

    public function changePassword(Request $request, $id) {

        $this->validate($request, [
            'password' => 'required|confirmed',
        ]);

        $admin = Admin::where('id', $id)->first();

        if(Hash::check($request->current_password, $admin->password)) {
            $admin->password = Hash::make($request->password);
            $admin->save();
            return response()->json(['message' => 'Password changed.']);
        }else {
            return response()->json(['not_match' => true, 'message' => 'Incorrect current password'], 200);
        }
    }

    public function activateAccount($id) {
        $admin = Admin::where('id', $id)->first();
        $admin->is_active = 1;
        $admin->save();
        return response()->json(['message' => 'Account Activated']);

    }

    public function deactivateAccount($id) {
        $admin = Admin::where('id', $id)->first();
        $admin->is_active = 0;
        $admin->save();
        return response()->json(['message' => 'Account Deactivated']);

    }

    public function activateAccounts() {
        Admin::where('access_level_id', '!=', 1)->update(['is_active' => 1]);
        return response()->json(['message' => 'All Account Activated']);

    }

    public function deactivateAccounts() {
        Admin::where('access_level_id', '!=', 1)->update(['is_active' => 0]);
        return response()->json(['message' => 'All Account Deactivated']);

    }


    

}
