<?php

namespace App\Http\Controllers\v1\Admin\MIS\UserAccounts;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentAccountController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {
        return response()->json(Student::with(['student_account', 'student_personal_info', 'student_school_info', 'organization_filter'])->latest()->get());
    }


    public function deactivateAccounts() {
        Student::where('is_active', 1)->update(['is_active' => 0]);
        return response()->json(['message' => 'All Account Deactivated']);

    }

}
