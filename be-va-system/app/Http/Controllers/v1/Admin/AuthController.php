<?php

namespace App\Http\Controllers\v1\Admin;

use App\Http\Controllers\Controller;
use App\Models\AccessLevel;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['login']]);
    }

    public function login(Request $request)
    {

        if (! $token = auth()->guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(['access_level' => auth('admin')->user()->access_level, 'role' => auth('admin')->user()->role, 'user' => auth('admin')->user(), 'organization' => auth('admin')->user()->organization]);
    }

    public function logout()
    {

        auth('admin')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('admin')->factory()->getTTL() * 60,
            'access_level' => auth('admin')->user()->access_level, 
            'role' => auth('admin')->user()->role,
            'user' => auth('admin')->user(),
            'organization' => auth('admin')->user()->organization
        ]);
    }
}
