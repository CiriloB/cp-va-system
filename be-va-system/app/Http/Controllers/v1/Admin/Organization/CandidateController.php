<?php

namespace App\Http\Controllers\v1\Admin\Organization;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use App\Models\StudentAccount;
use Illuminate\Http\Request;

class CandidateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {
        return response()->json(Candidate::with(['position', 'election', 'student.student_personal_info', 'student.student_account'])->get());
    }

    public function getCandidates(){
        // $presidents = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'President')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();

        // $vicepres = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'Vice President')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();

        // $secretary = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'Secretary')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();
        
        // $auditor = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'Auditor')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();

        // $treasurer = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'Treasurer')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();

        // $pio = Candidate::whereHas('position', function($query){
        //     $query->where('name', 'P.I.O')->where('election_id', request()->get('id'));
        // })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->latest()->get();


        // return response()->json([
        //     'president' => $presidents, 
        //     'vice_pres' => $vicepres,
        //     'auditor' => $auditor,
        //     'treasurer' => $treasurer,
        //     'secretary' => $secretary,
        //     'pio' => $pio,
        // ]);
        return response()->json(Candidate::where('election_id', request()->get('id'))->with(['position','student.student_account', 'student.student_personal_info', 'student.student_school_info.year_level'])->orderBy('position_id')->get());

    }

    public function store(Request $request) {

        $student = StudentAccount::where('student_number', $request->student_number)->with(['student'])->first();
        if(!empty($student)) {
            $election = Candidate::where('election_id', $request->election_id)->where('student_id', $student->id)->first();
            if(empty($election)) {
                Candidate::create([
                    'election_id' => $request->election_id,
                    'student_id' => $student->student->id,
                    'position_id' => $request->position_id,
                    'party_id' => $request->party_id,
                    'description' => $request->description
                ]);
                return response()->json(['message' => 'New Candidate added.'], 200);
            }else {
                return response()->json(['message' => 'Candidate already exist.'], 200);
            }
        }else {
            return response()->json(['message' => 'Student not found']);
        }
        
     
    }


    public function update(Request $request, $id) {

        $student_account = StudentAccount::where('student_number', $request->student_number)->with('student')->first();
        $candidate = Candidate::where('id', $id)->where('election_id', $request->election_id)->first();
        if(empty($candidate)) {
            return response()->json(['message' => 'Candidate not found.'], 200);
        } else {
            if($candidate->student_id != $student_account->student->id) {
                $candidate->student_id = $student_account->student->id;
            }
            $candidate->position_id = $request->position_id;
            $candidate->description = $request->description;
            $candidate->party_id = $request->party_id;
            $candidate->save();
            return response()->json(['message' => 'Candidate updated.'], 200);
        }
      
    }

    public function destroy($id) {
        Candidate::destroy($id);
        return response()->json(['message' => 'Candidate deleted.'], 200);
    }
}
