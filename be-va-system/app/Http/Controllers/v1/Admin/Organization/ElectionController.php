<?php

namespace App\Http\Controllers\v1\Admin\Organization;

use App\Http\Controllers\Controller;
use App\Models\Election;
use App\Models\Position;
use App\Models\Vote;
use App\Models\VoteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ElectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


   public function index() {
       return response()->json([
           'elections' => Election::where('organization_id', auth('admin')->user()->organization_id)->with(['candidates', 'candidates.student', 'candidates.student.student_personal_info', 'candidates.student.student_school_info.year_level'])->get(),
           'notStarted' => Election::where('organization_id', auth('admin')->user()->organization_id)->where('status', 'STARTED')->count() == 1 ? false : true
       ]);
   }

   public function store(Request $request) {
       
        $this->validate($request, [
            'title' => 'required',
        ]);

        Election::create([
            'title' => $request->title,
            'description' => $request->description,
            'organization_id' => auth('admin')->user()->organization_id,

        ]);

        return response()->json(['message' => 'Election saved.'], 200);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'organization_id' => auth('admin')->user()->organization_id
        ];

        Election::where('id', $id)->update($data);
        return response()->json(['message' => 'Election updated.'], 200);
    }

    public function destroy($id) {
        $election = Election::with(['candidates'])->where('id', $id)->first();
        if($election->candidates->count() > 0){
            foreach ($election->candidates as $candidates) {
                $candidates->delete();
            }
        }

        Election::destroy($id);

        return response()->json(['message' => 'Election deleted.'], 200);
    }

    public function startElection($id) {
        Election::where('organization_id', auth('admin')->user()->organization_id)->where('status', '!=', 'ENDED')->update(['status' => null]);
        $election = Election::where('organization_id', auth('admin')->user()->organization_id)->where('id', $id)->first();
        $election->status = "STARTED";
        $election->save();
        return response()->json(['message' => 'Election Started.', 'notStarted' => false], 200);
    }

    public function endElection($id) {
        $election_id = $id;
        $election = Election::where('organization_id', auth('admin')->user()->organization_id)->where('id', $id)->first();
        $election->status = "ENDED";
        $election->save();

        $candidates = Election::with(['candidates' => function($q)  use ($election_id){
            $id = $election_id;
            $q->withCount('vote_infos');
            $q->whereHas('election', function($query) use($id) {
                $query->where('organization_id',  auth('admin')->user()->organization_id)->where('id', $id);
            });
        }, 'candidates.student.student_personal_info', 'candidates.student.student_school_info.year_level'])->get();

        return response()->json(['message' => 'Election Ended.', 'notStarted' => false,  'vote_info' => $candidates], 200);

    }

    public function electionResult($id) {
        $election_id = $id;
        $candidates = Position::with(['candidates' => function($q) use($election_id) {
            $id = $election_id;
            $q->withCount('vote_infos');
            $q->whereHas('election', function($query) use($id) {
                $query->where('organization_id',  auth('admin')->user()->organization_id)->where('id', $id);
            });
        }, 'candidates.student.student_personal_info', 'candidates.student.student_school_info.year_level'])->get();

        return response()->json($candidates);
    }
}
