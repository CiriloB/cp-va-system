<?php

namespace App\Http\Controllers\v1\Admin\Organization;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAccount;
use Illuminate\Http\Request;

class StudentAccountController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function accountList() {
        return response()->json(Student::whereHas('organization_filter', function($q) {
            $q->where('ssc', auth('admin')->user()->organization_id)
            ->orWhere('year_level', auth('admin')->user()->organization_id)
            ->orWhere('college', auth('admin')->user()->organization_id)
            ->orWhere('course', auth('admin')->user()->organization_id);
        })->with(['student_account', 'student_personal_info', 'student_school_info', 'organization_filter'])->get());
    }

    public function approveAccount(Request $request, $id) {
        $student = StudentAccount::where('id', $id)->orWhere('student_number', $request->student_number)->first();
        $student->status = 'APPROVED';
        $student->save();
        return response()->json(['message' => 'Student account approved.'], 200);
    }

    public function disapproveAccount(Request $request, $id) {
        $student = StudentAccount::where('id', $id)->orWhere('student_number', $request->student_number)->first();
        $student->status = 'DISAPPROVED';
        $student->save();
        return response()->json(['message' => 'Student account disapproved.'], 200);
    }


}
