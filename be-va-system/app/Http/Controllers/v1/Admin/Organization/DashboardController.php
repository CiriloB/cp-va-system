<?php

namespace App\Http\Controllers\v1\Admin\Organization;
use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\Position;
use App\Models\Student;
use App\Models\VoteInfo;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //
    public function announcementCount() {
        return response()->json(Announcement::where('organization_id', auth('admin')->user()->organization_id)->count());
    }

    public function studentCount() {
        return response()->json(Student::whereHas('organization_filter', function($query) {
            $query->where('ssc', auth('admin')->user()->organization_id)
            ->orWhere('year_level', auth('admin')->user()->organization_id)
            ->orWhere('college', auth('admin')->user()->organization_id)
            ->orWhere('course', auth('admin')->user()->organization_id);
        })->count());
    }

    public function voteCount() {

        $candidates = Position::with(['candidates' => function($q) {
            $q->withCount('vote_infos');
            $q->whereHas('election', function($query) {
                $query->where('organization_id',  auth('admin')->user()->organization_id)->where('status', 'STARTED');
            });
        }, 'candidates.student.student_personal_info', 'candidates.student.student_school_info.year_level'])->get();
        
        $labels = array();
        $data = array();

        foreach($candidates[0]->candidates as $candidate) {
            $student_info = $candidate->student->student_personal_info->last_name . ", " . $candidate->student->student_personal_info->first_name . " (" . $candidate->student->student_school_info->year_level->year_level . ")";
            array_push($labels, $student_info);
            $vote_count = VoteInfo::where('candidate_id', $candidate->id)->count();
            array_push($data, $vote_count);

        }

        return response()->json(['candidates' => $candidates, 'presidents' => ['labels' => $labels, 'data' => $data]]);


    }
}
