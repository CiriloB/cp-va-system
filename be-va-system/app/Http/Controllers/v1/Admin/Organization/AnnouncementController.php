<?php

namespace App\Http\Controllers\v1\Admin\Organization;

use App\Http\Controllers\Controller;
use App\Mail\Student\AnnouncementMail;
use App\Models\Announcement;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AnnouncementController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index() {
        return response()->json(Announcement::where('organization_id', auth('admin')->user()->organization_id)->latest()->get());
    }

    public function store(Request $request) {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
        ]);

        $created_at = Announcement::create([
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
            'slug' => Str::slug($request->title),
            'organization_id' => auth('admin')->user()->organization_id
        ])->created_at;

        $full_name = auth('admin')->user()->first_name . " " . auth('admin')->user()->last_name;

        $content = [
            'posted_by' => $full_name,
            'posted_at' => $created_at,
            'organization' => auth('admin')->user()->organization->organization,
            'title' => $request->title,
            'content' => $request->content,

        ];
        $this->sendAnnouncementMail($content);
           
        return response()->json(['message' => 'New Announcement added.'], 200);

    }

    public function singleAnnouncement($slug) {
        return response()->json(Announcement::where('slug', $slug)->where('organization_id', auth('admin')->user()->organization_id)->first());
    }

    public function update(Request $request, $id) {
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
        ]);

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
            'slug' => Str::slug($request->title),
            'organization_id' => auth('admin')->user()->organization_id
        ];

        Announcement::where('id', $id)->update($data);
        return response()->json(['message' => 'Announcement updated.'], 200);
    }

    public function destroy($id) {
        Announcement::destroy($id);
        return response()->json(['message' => 'Announcement deleted.'], 200);
    }

    public function sendAnnouncementMail($content) {
        $students = Student::whereHas('organization_filter', function($query) {
            $query->where('ssc', 16)
            ->orWhere('year_level', 16)
            ->orWhere('college', 16)
            ->orWhere('course', 16);
        })->with(['student_account'])->get();

        $student_emails = [];
        foreach($students as $student) {
            array_push($student_emails, $student->student_account->email);
        }

        foreach ($student_emails as $recipient) {
            Mail::to($recipient)->send(new AnnouncementMail($content));
        }

    }
}
