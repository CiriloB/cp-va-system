<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Position;

class PositionController extends Controller
{
    //
    public function index() {
        return response()->json(Position::all());
    }
}
