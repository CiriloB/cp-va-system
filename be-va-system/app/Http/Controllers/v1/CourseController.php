<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Course;

class CourseController extends Controller
{
    //
    public function index() {
        return response()->json(Course::all());
    }
}
