<?php

namespace App\Http\Controllers\v1\Student;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\OrganizationFilter;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:student');
    }
    
    public function index(Request $request) {
        $announcements = null;
        if($request->organization_id == 0) {
            $organization_filter = OrganizationFilter::where('id', auth('student')->user()->student->organization_filter_id)->first();
            $organization_ids = array();
            array_push($organization_ids, $organization_filter->ssc);
            array_push($organization_ids, $organization_filter->year_level);
            array_push($organization_ids, $organization_filter->college);
            array_push($organization_ids, $organization_filter->course);
            $announcements = Announcement::whereIn('organization_id', $organization_ids)->with('organization')->latest()->get();
        }else {
            $announcements = Announcement::where('organization_id', $request->organization_id)->with('organization')->latest()->get();
           
        }
        return response()->json($announcements);
    }

    public function singleAnnouncement($slug) {
        return response()->json(Announcement::where('slug', $slug)->first());
    }

}
