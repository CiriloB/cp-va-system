<?php

namespace App\Http\Controllers\v1\Student;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\OrganizationFilter;
use App\Models\Position;
use App\Models\VoteInfo;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:student');
    }

    public function voteCount() {
        $candidates = Position::with(['candidates' => function($q) {
            $q->withCount('vote_infos');
            $q->with('election');
            $q->whereHas('election', function($query) {
                $query->where('organization_id',  request()->get('organization_id'))->where('status', 'STARTED');
                
            });
        }, 'candidates.student.student_personal_info', 'candidates.student.student_school_info.year_level'])->get();
        
        $labels = array();
        $data = array();


        foreach($candidates[0]->candidates as $candidate) {
                $student_info = $candidate->student->student_personal_info->last_name . ", " . $candidate->student->student_personal_info->first_name . " (" . $candidate->student->student_school_info->year_level->year_level . ")";
                array_push($labels, $student_info);
                $vote_count = VoteInfo::where('candidate_id', $candidate->id)->count();
                array_push($data, $vote_count);

        }
        return response()->json(['candidates' => $candidates, 'presidents' => ['labels' => $labels, 'data' => $data]]);
    }

    public function getLatestAnnouncements() {
        $organization_filter = auth('student')->user()->student->organization_filter;
        $organization_ids = array();
        array_push($organization_ids, $organization_filter->ssc);
        array_push($organization_ids, $organization_filter->year_level);
        array_push($organization_ids, $organization_filter->college);
        array_push($organization_ids, $organization_filter->course);
        $announcements = Announcement::whereIn('organization_id', $organization_ids)->latest()->take(5)->get();
        return response()->json($announcements);
    }
}
