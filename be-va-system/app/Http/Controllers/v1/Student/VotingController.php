<?php

namespace App\Http\Controllers\v1\Student;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use App\Models\Election;
use App\Models\Organization;
use App\Models\OrganizationFilter;
use App\Models\Student;
use App\Models\Vote;
use App\Models\VoteInfo;
use Illuminate\Http\Request;

class VotingController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:student');
    }

    public function index() {
        return response()->json(Election::where('status', 'STARTED')->with('candidates')->all());
    }


    public function getElections() {
        $student = Student::where('id', auth('student')->user()->id)->first();
        $organization_filter = OrganizationFilter::where('id', $student->organization_filter_id)->first();
        $organizations = array();
        array_push($organizations, $organization_filter->ssc);
        array_push($organizations, $organization_filter->year_level);
        array_push($organizations, $organization_filter->college);
        array_push($organizations, $organization_filter->course);
        $elections = Election::where('status', 'STARTED')->whereIn('organization_id', $organizations)->with(['votes' => function($query){
            $query->where('student_id', auth('student')->user()->id);
           }])->get();
        return response()->json($elections);
    }

    public function getOrganizations() {

        $organization_filter = OrganizationFilter::where('id',auth('student')->user()->student->organization_filter_id)->first();
        $organization_ids = array();
        array_push($organization_ids, $organization_filter->ssc);
        array_push($organization_ids, $organization_filter->year_level);
        array_push($organization_ids, $organization_filter->college);
        array_push($organization_ids, $organization_filter->course);
        $organizations = Organization::whereIn('id', $organization_ids)->get();
        return response()->json($organizations);
    }

    public function getCandidates() {

        $presidents = Candidate::whereHas('position', function($query){
            $query->where('id', 1)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();
        
        $vicepres = Candidate::whereHas('position', function($query){
            $query->where('id', 2)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();

        $secretary = Candidate::whereHas('position', function($query){
            $query->where('id', 3)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();
        
        $auditor = Candidate::whereHas('position', function($query){
            $query->where('id', 4)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();

        $treasurer = Candidate::whereHas('position', function($query){
            $query->where('id', 5)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();

        $pio = Candidate::whereHas('position', function($query){
            $query->where('id', 6)->where('election_id', request()->get('id'));
        })->with(['student', 'student.student_personal_info', 'student.student_school_info.year_level'])->get();

        return response()->json([
            'president' => $presidents, 
            'vice_pres' => $vicepres,
            'auditor' => $auditor,
            'treasurer' => $treasurer,
            'secretary' => $secretary,
            'pio' => $pio,
        ]);
    }

    public function submitVote(Request $request) {

        $student = Vote::where('student_id', auth('student')->user()->student->id)->where('election_id', $request->election_id)->first();

        if(empty($student)) {
    
            if(count($request->pio) > 2){

                return response()->json(['message' => 'Maxed vote in this position. Please choose at least 2 candidates for PIO']);        
               
            }else if(count($request->auditor) > 2)  {

                return response()->json(['message' => 'Maxed vote in this position. Please choose at least 2 candidates for Auditor']); 
                       
            } else {


                $vote = [
                    'student_id' => auth('student')->user()->student->id,
                    'election_id' => $request->election_id
                ];

                $vote = Vote::create($vote);


                $vote_info = [
                    'election_id' => $request->election_id,
                    'vote_id' => $vote->id,
                    'candidate_id' => $request->president
                ];

                VoteInfo::create($vote_info);


                $vote_info = [
                    'election_id' => $request->election_id,
                    'vote_id' => $vote->id,
                    'candidate_id' => $request->vicepresident
                ];

                VoteInfo::create($vote_info);

                $vote_info = [
                    'election_id' => $request->election_id,
                    'vote_id' => $vote->id,
                    'candidate_id' => $request->secretary
                ];

                VoteInfo::create($vote_info);

                foreach($request->auditor as $auditor) {
                    $vote_info = [
                        'election_id' => $request->election_id,
                        'vote_id' => $vote->id,
                        'candidate_id' => $auditor
                    ];

                    VoteInfo::create($vote_info);
                }

                $vote_info = [
                    'election_id' => $request->election_id,
                    'vote_id' => $vote->id,
                    'candidate_id' => $request->treasurer
                ];

                VoteInfo::create($vote_info);

                
                foreach($request->pio as $pio) {
                    $vote_info = [
                        'election_id' => $request->election_id,
                        'vote_id' => $vote->id,
                        'candidate_id' => $pio
                    ];

                    VoteInfo::create($vote_info);
                }
            }


            return response()->json(['message' => 'Your vote successfully submitted.']);  
                  
        }else {

            return response()->json(['message' => 'You are already voted in this election']);

        }

    }
}
