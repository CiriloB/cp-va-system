<?php

namespace App\Http\Controllers\v1\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\Student\ChangePasswordRequest;
use App\Mail\PasswordResetMail;
use App\Mail\Student\EmailVerificationMail;
use App\Models\College;
use App\Models\Course;
use App\Models\OrganizationFilter;
use App\Models\Student;
use App\Models\StudentAccount;
use App\Models\StudentPersonalInfo;
use App\Models\StudentSchoolInfo;
use App\Models\YearLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
     //

     public function __construct()
     {
         $this->middleware('auth:student', ['except' => ['login', 'store', 'resetPasswordRequest', 'resetForm','resetPassword']]);
     }
     
     public function login(Request $request)
     {
 
         if (! $token = auth()->guard('student')->attempt(['student_number' => $request->student_number, 'password' => $request->password])) {
             return response()->json(['error' => 'Unauthorized'], 401);
         }
 
         return auth('student')->user()->status == 'PENDING' ? response()->json(['student_status' => auth('student')->user()->status], 200)  : $this->respondWithToken($token);
     }
 
     public function store(Request $request)
     {
 
         $this->validate($request, [
             'student_number' => 'required',
             'email' => 'required',
             'password' => 'required',
             'first_name' => 'required',
             'middle_name' => 'required',
             'last_name' => 'required',
         ]);
 
         $student_account = StudentAccount::create([
             'student_number' => $request->student_number,
             'email' => $request->email,
             'password' => Hash::make($request->password),
         ]);
 
         $student_personal_info = StudentPersonalInfo::create([
             'first_name' => $request->first_name,
             'middle_name' => $request->middle_name,
             'last_name' => $request->last_name,
         ]);
 
         $student_school_info = StudentSchoolInfo::create([
             'college_id' => $request->college_id,
             'course_id' => $request->course_id,
             'semester_id' => 1,
             'year_level_id' => $request->year_level_id,
         ]);
 
         $organization_filter = OrganizationFilter::create([
             'year_level' => YearLevel::where('id', $request->year_level_id)->first()->organization_id,
             'college' => College::where('id', $request->college_id)->first()->organization_id,
             'course' => Course::where('id', $request->course_id)->first()->organization_id
         ]);
 
         Student::create([
             'student_account_id' => $student_account->id,
             'student_personal_info_id' => $student_personal_info->id,
             'student_school_info_id' => $student_school_info->id,
             'organization_filter_id' => $organization_filter->id
         ]);
 
 
         return response()->json(['message' => "Account created successfuly! Wait for admin's approval"], 200);
     }
 
 
     public function updateAccount(Request $request) {
 
         $this->validate($request, [
             'email' => 'required',
             'first_name' => 'required',
             'middle_name' => 'required',
             'last_name' => 'required',
             'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
         ]);
 
         $student_account = StudentAccount::where('student_number', auth('student')->user()->student_number)->first();
         if($student_account->email != $request->email) {
             $student_account->email = $request->email;
             $student_account->is_email_verified = 0;
         }
 
         $student_personal_info = StudentPersonalInfo::where('id', auth('student')->user()->student->student_personal_info_id)->first();
 
         $profile_image = $request->file('profile_image');
         $profile_image_name = $profile_image->getClientOriginalExtension();
         $profile_image_name = $student_account->student_number . '.' . $profile_image_name;
 
         $profile_image->move(public_path('/images'), $profile_image_name);
 
         $student_personal_info->first_name = $request->first_name;
         $student_personal_info->middle_name = $request->middle_name;
         $student_personal_info->last_name = $request->last_name;
         $student_personal_info->profile_image = '/images/' . $profile_image_name;
 
         $student_account->save();
         $student_personal_info->save();
         return response()->json($request);
 
     }

     public function updateAccountInfo(Request $request) {

        $this->validate($request, [
            'email' => 'required',
        ]);

        $student_account = StudentAccount::where('student_number', auth('student')->user()->student_number)->first();
        $student_account->email = $request->email;

        $student_school_info = StudentSchoolInfo::where('id', $student_account->id)->first();
        $student_school_info->college_id = $request->college_id;
        $student_school_info->course_id = $request->course_id;
        $student_school_info->year_level_id = $request->year_level_id;

        $student_account->save();
        $student_school_info->save();

        // Change the org filter
        $organization_filter = OrganizationFilter::where('id', auth('student')->user()->id)->first();
        $organization_filter->year_level = YearLevel::where('id', $request->year_level_id)->first()->organization_id;
        $organization_filter->college = College::where('id', $request->college_id)->first()->organization_id;
        $organization_filter->course = Course::where('id', $request->course_id)->first()->organization_id;
        $organization_filter->save();

        $student = Student::where('id', auth('student')->user()->id)->first();
        $student->is_active = true;
        $student->save();


        return response()->json(['message' => 'Account updated.']);
     }
 
     public function resetPasswordRequest(Request $request) {
 
         $this->validate($request, [
             'email' => 'required|email'
         ]);
 
         $account = StudentAccount::where('email', $request->email)->first();
         if(!empty($account)) {
             $content = [
                 'email' => $request->email,
                 'student_number' => $account->student_number
             ];
             Mail::to($request->email)->send(new PasswordResetMail($content));
         }else {
             return response()->json(['message' => 'Email not found.'],404);
         }
       
     }
 
     public function resetForm($email) {
         return view('student.password_reset', compact('email'));
     }
     
     public function resetPassword(PasswordResetRequest $request, $email) {
         $account = StudentAccount::where('email', $email)->first();
         if(!empty($account)) {
            $account->password = bcrypt($request->password);
            $account->save();
            return view('student.message.password_reset_success');
         }else {
            return view('student.message.password_reset_failed');
         }
     
     }

     public function emailVerificationRequest(Request $request) {

        $content = [
            'email' => auth('student')->user()->email,
            'student_number' => auth('student')->user()->student_number,
            'token' => $request->token
        ];

        Mail::to(auth('student')->user()->email)->send(new EmailVerificationMail($content));
        return response()->json(['message' => 'Email verification sent.']);
     }

     public function changePassword(ChangePasswordRequest $request) {
        $account = StudentAccount::where('student_number', auth('student')->user()->student_number)->first();
        $match = false;
        if(!empty($account)) {
            if(Hash::check($request->current_password, $account->password)) {
                $account->password = bcrypt($request->password);
                $account->save();
                return response()->json(['match' => true,'message' => 'Password successfully changed.']);
            }else {
                return response()->json(['match' => false, 'message' => 'Current password does not match']);
            }
        }else {
            return response()->json(['message' => 'Email not found.'],404);
        }
     }
 
     public function verifyEmail(Request $request) {

        $account = StudentAccount::where('email', $request->email)->first();
        if(!empty($account)) {
            $account->is_email_verified = true;
            $account->save();
            return view('student.message.email_verify_success');
        }
        return view('student.message.email_verify_failed');
     }

     public function me()
     {
         return response()->json(Student::with(['student_account','student_personal_info', 'student_school_info', 'organization_filter'])->where('id', Auth::id())->first());
     }
 
     public function logout()
     {
         auth('student')->logout();
         return response()->json(['message' => 'User logged out successfully!']);
     }
 
     public function refresh()
     {
         return $this->respondWithToken(auth()->refresh());
     }
 
     protected function respondWithToken($token)
     {
         return response()->json([
             'access_token' => $token,
             'token_type' => 'bearer',
             'expires_in' => auth('student')->factory()->getTTL() * 60,
             'is_active' => auth('student')->user()->student->is_active,
         ]);
     }
}
