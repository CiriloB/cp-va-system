<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\College;

class CollegeController extends Controller
{
    //
    public function index() {
        return response()->json(College::all());
    }
}
