<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Semester;

class SemesterController extends Controller
{
    //

    public function index() {
        return response()->json(Semester::all());
    }
}
