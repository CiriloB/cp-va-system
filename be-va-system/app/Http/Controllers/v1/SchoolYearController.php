<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\SchoolYear;

class SchoolYearController extends Controller
{
    //
    public function index() {
        return response()->json(SchoolYear::all());
    }
}
