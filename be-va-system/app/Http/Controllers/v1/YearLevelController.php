<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\YearLevel;

class YearLevelController extends Controller
{
    //
    public function index() {
        return response()->json(YearLevel::all());
    }
}
