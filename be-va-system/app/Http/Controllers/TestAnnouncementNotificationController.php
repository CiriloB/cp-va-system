<?php

namespace App\Http\Controllers;

use App\Mail\Student\AnnouncementMail;
use App\Models\Student;
use Illuminate\Support\Facades\Mail;

class TestAnnouncementNotificationController extends Controller
{
    //
    public function sendAnnouncementMailToStudents() {
        // digits
        $students = Student::whereHas('organization_filter', function($query) {
            $query->where('ssc', 16)
            ->orWhere('year_level', 16)
            ->orWhere('college', 16)
            ->orWhere('course', 16);
        })->with(['student_account'])->get();

        $student_emails = [];
        foreach($students as $student) {
            array_push($student_emails, $student->student_account->email);
        }

        $announcement = [
            'title' => 'Random Announcement',
            'content' => '<h1>Kape</h1>',
            'admin_name' => 'John Doe'
        ];

        foreach ($student_emails as $recipient) {
            Mail::to($recipient)->send(new AnnouncementMail($announcement));
        }

        return response()->json(['message' => 'Email send']);
    }
}
