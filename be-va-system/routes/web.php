<?php

use App\Http\Controllers\TestAnnouncementNotificationController;
use App\Http\Controllers\v1\Student\AuthController as StudentAuthController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('student.message.password_reset_failed');
});

Route::get('student/verify-email', [StudentAuthController::class, 'verifyEmail']);
Route::get('/password-reset/{email}', [StudentAuthController::class, 'resetForm']);
Route::post('/reset-password/{email}', [StudentAuthController::class, 'resetPassword']);
Route::get('/test', [TestController::class, 'index']);


Route::get('/test', [TestAnnouncementNotificationController::class, 'sendAnnouncementMailToStudents']);
Route::get('/announcement', function() {
    return view('student.welcome');
});





