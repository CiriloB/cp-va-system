<?php

use App\Http\Controllers\PartyController;
use App\Http\Controllers\v1\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\v1\Student\AuthController as StudentAuthController;

use App\Http\Controllers\v1\Admin\MIS\ {
    DashboardController as MISDashboardController,
    UserAccounts\StudentAccountController as MISStudentAccountController,
    UserAccounts\OrganizationAccountController as MISOrganizationAccountController
};

 use App\Http\Controllers\v1\Admin\Organization\ {
     AnnouncementController as OrganizationAnnouncementController,
     CandidateController as OrganizationCandidateController,
     DashboardController as OrganizationDashboardController,
     ElectionController as OrganizationElectionController,
     StudentAccountController as OrganizationStudentAccountController
};

use App\Http\Controllers\v1\Student\ {
    AnnouncementController as StudentAnnouncementController,
    DashboardController as StudentDashboardController,
    VotingController as StudentVotingController
};

use App\Http\Controllers\v1\ {
    CollegeController,
    CourseController,
    PositionController,
    SchoolYearController,
    SemesterController,
    YearLevelController
};

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::group(['prefix' => 'admin'], function (){
        Route::put('updateAccount', [AdminAuthController::class, 'updateAccount']);
        Route::post('login', [AdminAuthController::class, 'login']);
        Route::post('logout', [AdminAuthController::class, 'logout']);
        Route::post('me', [AdminAuthController::class, 'me']);
    });

    Route::group(['prefix' => 'student'], function (){
        Route::post('store', [StudentAuthController::class, 'store']);
        Route::post('email-verification-request', [StudentAuthController::class, 'emailVerificationRequest']);
        Route::post('reset-password-request', [StudentAuthController::class, 'resetPasswordRequest']);
        Route::post('change-password', [StudentAuthController::class, 'changePassword']);
        Route::post('reset-password/{email}', [StudentAuthController::class, 'resetPassword']);
        Route::post('update-account', [StudentAuthController::class, 'updateAccount']);
        Route::post('update-account-info', [StudentAuthController::class, 'updateAccountInfo']);
        Route::post('login', [StudentAuthController::class, 'login']);
        Route::post('logout', [StudentAuthController::class, 'logout']);
        Route::post('me', [StudentAuthController::class, 'me']);
    });

});

Route::group(['middleware' => 'api'], function (){

    Route::group(['prefix' => 'mis'], function () {
        Route::get('ongoing-election-count', [MISDashboardController::class, 'ongoingElectionCount']);
        Route::get('organization-account-count', [MISDashboardController::class, 'organizationAccountCount']);
        Route::get('organization-count', [MISDashboardController::class, 'organizationCount']);
        Route::get('student-account-count', [MISDashboardController::class, 'studentAccountCount']);
        Route::get('get-organizations', [MISDashboardController::class, 'getOrganizations']);
        Route::get('get-roles', [MISDashboardController::class, 'getRoles']);
        Route::apiResource('organization-accounts', MISOrganizationAccountController::class);
        Route::put('organization-change-password/{id}', [MISOrganizationAccountController::class, 'changePassword']);
        Route::put('organization-activate-account/{id}', [MISOrganizationAccountController::class, 'activateAccount']);
        Route::put('organization-activate-accounts', [MISOrganizationAccountController::class, 'activateAccounts']);
        Route::put('organization-deactivate-account/{id}', [MISOrganizationAccountController::class, 'deactivateAccount']);
        Route::put('organization-deactivate-accounts', [MISOrganizationAccountController::class, 'deactivateAccounts']);
        Route::put('student-deactivate-accounts', [MISStudentAccountController::class, 'deactivateAccounts']);
        Route::apiResource('student-accounts', MISStudentAccountController::class);
        // Route::get('get-candidates', [OrganizationCandidateController::class, 'getCandidates']);
    });

    Route::group(['prefix' => 'organization'], function () {
        Route::get('announcement-count', [OrganizationDashboardController::class, 'announcementCount']);
        Route::get('student-count', [OrganizationDashboardController::class, 'studentCount']);
        Route::get('vote-count', [OrganizationDashboardController::class, 'voteCount']);
        Route::get('election-result/{id}', [OrganizationElectionController::class, 'electionResult']);
        Route::get('account-list', [OrganizationStudentAccountController::class, 'accountList']);
        Route::put('approve-account/{id}', [OrganizationStudentAccountController::class, 'approveAccount']);
        Route::put('disapprove-account/{id}', [OrganizationStudentAccountController::class, 'disapproveAccount']);
        Route::apiResource('announcements', OrganizationAnnouncementController::class);
        Route::get('single-announcement/{slug}', [OrganizationAnnouncementController::class, 'singleAnnouncement']);
        Route::apiResource('elections', OrganizationElectionController::class);
        Route::put('start-election/{id}', [OrganizationElectionController::class, 'startElection']);
        Route::put('end-election/{id}', [OrganizationElectionController::class, 'endElection']);
        Route::apiResource('candidates', OrganizationCandidateController::class);
        Route::get('get-candidates', [OrganizationCandidateController::class, 'getCandidates']);
    });

    Route::group(['prefix' => 'student'], function () {
        Route::get('ongoing-election-count', [StudentDashboardController::class, 'ongoingElectionCount']);
        Route::get('get-latest-announcements', [StudentDashboardController::class, 'getLatestAnnouncements']);
        Route::get('vote-count', [StudentDashboardController::class, 'voteCount']);
        Route::get('get-announcements', [StudentAnnouncementController::class, 'index']);
        Route::get('single-announcement-student/{slug}', [StudentAnnouncementController::class, 'singleAnnouncement']);
        Route::get('get-candidates', [StudentVotingController::class, 'getCandidates']);
        Route::get('get-elections', [StudentVotingController::class, 'getElections']);
        Route::get('get-organizations', [StudentVotingController::class, 'getOrganizations']);
        Route::post('submit-vote', [StudentVotingController::class, 'submitVote']);
    });

});

Route::apiResource('colleges', CollegeController::class)->only(['index']);
Route::apiResource('courses', CourseController::class)->only(['index']);
Route::apiResource('courses', CourseController::class)->only(['index']);
Route::apiResource('year_levels', YearLevelController::class)->only(['index']);
Route::apiResource('parties', PartyController::class)->only(['index']);
Route::apiResource('positions', PositionController::class)->only(['index']);
Route::apiResource('school_years', SchoolYearController::class)->only(['index']);
Route::apiResource('semesters', SemesterController::class)->only(['index']);