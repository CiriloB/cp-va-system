import Vue from 'vue'
import VueRouter from 'vue-router'
import NotFound from '../views/NotFound.vue'
import Landing from '../views/Landing.vue'

import Announcement from '../views/Student/Announcement.vue'
import ViewAnnouncement from '../views/Student/ViewAnnouncement.vue'
import Dashboard from '../views/Student/Dashboard.vue'
import Login from '../views/Login.vue'
import Forgot from '../views/Forgot.vue'
import Signup from '../views/Signup.vue'
import Pending from '../views/Pending.vue'
import UpdateStudentInfo from '../views/UpdateStudentInfo.vue'
import Student from '../views/Student/Student.vue'
import Vote from '../views/Student/Vote.vue'
import Voting from '../views/Student/Voting.vue'
import Candidates from '../views/Student/Candidates.vue'
import Candidate from '../views/Student/Candidate.vue'
import AccountSetting from '../views/Student/AccountSetting.vue'

import OrganizationAdmin from '../views/Admin/Organization/Organization.vue'
import OrganizationAdminAccountRequest from '../views/Admin/Organization/AccountRequest.vue'
import OrganizationAdminAnnouncement from '../views/Admin/Organization/Announcement.vue'
import OrganizationAdminDashboard from '../views/Admin/Organization/Dashboard.vue'
import OrganizationAdminElection from '../views/Admin/Organization/Election.vue'  
import OrganizationAdminElectionResult from '../views/Admin/Organization/ElectionResult.vue'  
import OrganizationAdminManageCandidates from '../views/Admin/Organization/ManageCandidates.vue'
import OrganizationAdminViewAnnouncement from '../views/Admin/Organization/ViewAnnouncement.vue'
import OrganizationAccountSettings from '../views/Admin/Organization/AccountSettings.vue'

// MIS
import MISAdmin from '../views/Admin/MIS/MIS.vue'
import MISDashboard from '../views/Admin/MIS/Dashboard.vue';
import MISOrganizationAccount from '../views/Admin/MIS/OrganizationAccount.vue';
import MISStudentAccount from '../views/Admin/MIS/StudentAccount.vue';
import MISSettings from '../views/Admin/MIS/Settings.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: "*",
    component: NotFound
  },
  {
    path: "/",
    component: Landing
  },
  {
    path: "/login", name: 'Login', component: Login, meta: { hasUser: true }
  },
  {
    path: "/forgot", name: 'Forgot Password', component: Forgot, meta: { hasUser: true }
  },
  {
    path: "/signup", component: Signup, meta: { hasUser: true }
  },
  {
    path: "/pending", component: Pending, meta: { hasUser: true }

  },
  {
    path: "/update-student-info", component: UpdateStudentInfo, meta: { hasUser: true }

  },
  {
    path: '/student',
    component: Student,
    meta: { requiresLogin: true },
    children: [
      { path: "announcement", name: 'Student Announcement', component: Announcement },
      { path: "announcement/:slug", name: 'ViewAnnouncement', component: ViewAnnouncement },
      { path: "dashboard", component: Dashboard },
      { path: "vote", component: Vote },
      { path: "voting", component: Voting },
      { path: "candidates/:election_id", name: "ViewCandidates", component: Candidates },
      { path: "candidate/:candidate_id", component: Candidate },
      { path: "settings", name: 'Student Account Settings', component: AccountSetting },
      { path: "", redirect: "dashboard" }
    ],
  },
  {
    path: '/mis-admin',
    component: MISAdmin,
    meta: { requiresLogin: true },
    children: [
      { path: "dashboard", name: 'MISDashboard', component: MISDashboard },
      { path: "organization-account", name: 'MISOrganizationAccount', component: MISOrganizationAccount },
      { path: "student-account", name: 'MISStudentAccounts', component: MISStudentAccount },
      { path: "setting", name: 'MISSettings', component: MISSettings },
      { path: "", redirect: "dashboard" }
    ]
  },
  {
    path: '/organization-admin',
    component: OrganizationAdmin,
    meta: { requiresLogin: true },
    children: [
      { path: "announcement", name: 'Announcement', component: OrganizationAdminAnnouncement },
      { path: "announcement/:slug", name: 'View Announcement', component: OrganizationAdminViewAnnouncement },
      { path: "dashboard", component: OrganizationAdminDashboard },
      { path: "manage-candidates/:election_id", name: 'ManageCandidates', component: OrganizationAdminManageCandidates },
      { path: "election", component: OrganizationAdminElection },
      { path: "election-result/:election_id", name: 'OrganizationAdminElectionrResult', component: OrganizationAdminElectionResult },
      { path: "settings", name: 'OrganizationSettings', component: OrganizationAccountSettings },
      { path: "account-request", component: OrganizationAdminAccountRequest },
      { path: "", redirect: "dashboard" }
    ],
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('access_token')) {
    next({ path: '/login' })
  }
  else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('access_token') && localStorage.getItem('isMIS')) {
    next({ path: '/mis-admin/' });
  }
  else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('access_token') && localStorage.getItem('isORG')) {
    next({ path: '/organization-admin/' });
  }
  else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('access_token') && localStorage.getItem('isUser')) {
    next({ path: '/student/' });
  }
  else {
    next();
  }
});

export default router