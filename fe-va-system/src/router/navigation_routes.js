const admin = [
    { title: 'Dashboard', icon: 'grid', path: 'dashboard' },
    { title: 'Organization', icon: 'briefcase', path: 'organization-account' },
    { title: 'Student', icon: 'people', path: 'student-account' },
    // { title: 'Organization', icon: 'briefcase-fill', path: 'organization' },

]

const org_admin = [
    { title: 'Dashboard', icon: 'grid', path: '/organization-admin/dashboard', access_level_id: 2, role_id: null },
    { title: 'Announcement', icon: 'inbox', path: '/organization-admin/announcement', access_level_id: 2, role_id: 2 },
    { title: 'Account Request', icon: 'people', path: '/organization-admin/account-request', access_level_id: 2, role_id: 3 },
    { title: 'Election', icon: 'bar-chart', path: '/organization-admin/election', access_level_id: 2, role_id: 3 },
]

module.exports = { admin, org_admin }