import axios from '@/axios'

const ORGANIZATION_ADMIN = "organization"

export default {
    namespaced: true,
    state: {
        elections: [],
        positions: [],
        candidates: {},
        students: {},
        notStarted: null,
        voting_count: {}
    },
    mutations: {
        SET_ELECTIONS: (state, elections) => state.elections = elections,
        SET_STUDENTS: (state, students) => state.students = students,
        SET_POSITIONS: (state, positions) => state.positions = positions,
        SET_CANDIDATES: (state, candidates) => state.candidates = candidates,
        SET_ONGOING_ELECTION_STATUS: (state, notStarted) => state.notStarted = notStarted,
        UPDATE_CANDIDATES(state, id) {
            state.candidates = state.candidates.filter(candidate => {
                return candidate.election_id == id;
            })
        },
        SET_VOTE_COUNT: (state, count) => state.voting_count = count,

    },
    getters: {
        GET_ELECTIONS: (state) => state.elections,
        GET_POSITIONS: (state) => state.positions,
        GET_STUDENTS: (state) => state.students,
        GET_CANDIDATES: (state) => state.candidates,
        GET_ONGOING_ELECTION_STATUS: (state) => state.notStarted,
        GET_VOTE_COUNT: (state) => state.voting_count,

    },
    actions: {
        async FETCH_STUDENTS({ commit }) {
            await axios.get(`${ORGANIZATION_ADMIN}/account-list?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_STUDENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        STORE_ELECTION: async ({ commit }, data) => {
            const res = await axios.post(`${ORGANIZATION_ADMIN}/elections?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        DELETE_ELECTION: async ({ commit }, id) => {
            const res = await axios.delete(`${ORGANIZATION_ADMIN}/elections/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        UPDATE_ELECTION: async ({ commit }, { data, id }) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/elections/${id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        START_ELECTION: async ({ commit }, id) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/start-election/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        END_ELECTION: async ({ commit }, id) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/end-election/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        FETCH_ELECTIONS: async ({ commit }) => {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/elections?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ELECTIONS", response.data.elections);
                    commit("SET_ONGOING_ELECTION_STATUS", response.data.notStarted);

                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STORE_CANDIDATE: async ({ commit }, data) => {
            const res = await axios.post(`${ORGANIZATION_ADMIN}/candidates?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        UPDATE_CANDIDATE: async ({ commit }, data) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/candidates/${data.id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        DELETE_CANDIDATE: async ({ commit }, id) => {
            const res = await axios.delete(`${ORGANIZATION_ADMIN}/candidates/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        FETCH_CANDIDATES: async ({ commit }, id) => {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/get-candidates?id=${id}&token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_CANDIDATES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async FETCH_VOTE_COUNT({ commit }, id) {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/election-result/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_VOTE_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}