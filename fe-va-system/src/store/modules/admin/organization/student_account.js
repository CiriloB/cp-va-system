import axios from '@/axios'

const ORGANIZATION_ADMIN = "organization"

export default {
    namespaced: true,
    state: {
        students: [],
    },
    mutations: {
        SET_STUDENTS: (state, students) => state.students = students,
    },
    getters: {
        GET_STUDENTS: (state) => state.students,
    },
    actions: {
        async FETCH_STUDENTS({ commit }) {
            await axios.get(`${ORGANIZATION_ADMIN}/account-list?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_STUDENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        APPROVE: async ({ commit }, { data, id }) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/approve-account/${id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        DISAPPROVE: async ({ commit }, { data, id }) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/disapprove-account/${id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

    }

}