import axios from '@/axios'

const ORGANIZATION_ADMIN = "organization"

export default {
    namespaced: true,
    state: {
        announcements: {},
        announcement: {},
    },
    mutations: {
        SET_ANNOUNCEMENTS: (state, announcements) => state.announcements = announcements,
        SET_ANNOUNCEMENT: (state, announcement) => state.announcement = announcement,
    },
    getters: {
        GET_ANNOUNCEMENTS: (state) => state.announcements,
        GET_ANNOUNCEMENT: (state) => state.announcement,
    },
    actions: {
        STORE_ANNOUNCEMENT: async ({ commit }, data) => {
            const res = await axios.post(`${ORGANIZATION_ADMIN}/announcements?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        DELETE_ANNOUNCEMENT: async ({ commit }, id) => {
            const res = await axios.delete(`${ORGANIZATION_ADMIN}/announcements/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        UPDATE_ANNOUNCEMENT: async ({ commit }, { data, id }) => {
            const res = await axios.put(`${ORGANIZATION_ADMIN}/announcements/${id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        FETCH_ANNOUNCEMENT: async ({ commit }) => {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/announcements?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ANNOUNCEMENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        SINGLE_ANNOUNCEMENT: async ({ commit }, slug) => {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/single-announcement/${slug}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ANNOUNCEMENT", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}