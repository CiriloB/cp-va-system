import axios from '@/axios'

const ORGANIZATION_ADMIN = "organization"

export default {
    namespaced: true,
    state: {
        announcement_count: 0,
        student_count: 0,
        voting_count: 0

    },
    mutations: {
        SET_STUDENT_COUNT: (state, count) => state.student_count = count,
        SET_ANNOUNCEMENT_COUNT: (state, count) => state.announcement_count = count,
        SET_VOTE_COUNT: (state, count) => state.voting_count = count,

    },
    getters: {
        GET_STUDENT_COUNT: (state) => state.student_count,
        GET_ANNOUNCEMENT_COUNT: (state) => state.announcement_count,
        GET_ELECTION_COUNT: (state) => state.election_count,
        GET_VOTE_COUNT: (state) => state.voting_count,
    },
    actions: {
        // Counts
        async FETCH_ANNOUNCEMENT_COUNT({ commit }) {
            await axios.get(`${ORGANIZATION_ADMIN}/announcement-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ANNOUNCEMENT_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_STUDENT_COUNT({ commit }) {
            await axios.get(`${ORGANIZATION_ADMIN}/student-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_STUDENT_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_VOTE_COUNT({ commit }) {
            const res = await axios.get(`${ORGANIZATION_ADMIN}/vote-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_VOTE_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}