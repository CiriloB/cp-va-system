import axios from '@/axios'

const MIS = 'mis'
export default {
    namespaced: true,
    state: {
        ongoing_election_count: 0,
        organization_account_count: 0,
        organization_count: 0,
        student_account_count: 0,
        organizations: {},
        roles: {}
    },
    mutations: {
        SET_ONGOING_ELECTION_COUNT: (state, ongoing_election_count) => state.ongoing_election_count = ongoing_election_count,
        SET_ORGANIZATION_ACCOUNT_COUNT: (state, organization_account_count) => state.organization_account_count = organization_account_count,
        SET_ORGANIZATION_COUNT: (state, organization_count) => state.organization_count = organization_count,
        SET_STUDENT_ACCOUNT_COUNT: (state, student_account_count) => state.student_account_count = student_account_count,
        SET_ORGANIZATIONS: (state, organizations) => state.organizations = organizations,
        SET_ROLES: (state, roles) => state.roles = roles,

    },
    getters: {
        GET_ONGOING_ELECTION_COUNT: (state) => state.ongoing_election_count,
        GET_ORGANIZATION_ACCOUNT_COUNT: (state) => state.organization_account_count,
        GET_ORGANIZATION_COUNT: (state) => state.organization_count,
        GET_STUDENT_ACCOUNT_COUNT: (state) => state.student_account_count,
        GET_ORGANIZATIONS: (state) => state.organizations,
        GET_ROLES: (state) => state.roles,

    },
    actions: {
        FETCH_ONGOING_ELECTION_COUNT: async ({ commit }) => {
            await axios.get(`${MIS}/ongoing-election-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ONGOING_ELECTION_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_ORGANIZATION_ACCOUNT_COUNT: async ({ commit }) => {
            await axios.get(`${MIS}/organization-account-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ORGANIZATION_ACCOUNT_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_ORGANIZATION_COUNT: async ({ commit }) => {
            await axios.get(`${MIS}/organization-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ORGANIZATION_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_STUDENT_ACCOUNT_COUNT: async ({ commit }) => {
            await axios.get(`${MIS}/student-account-count?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_STUDENT_ACCOUNT_COUNT", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_ORGANIZATIONS: async ({ commit }) => {
            await axios.get(`${MIS}/get-organizations?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ORGANIZATIONS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_ROLES: async ({ commit }) => {
            await axios.get(`${MIS}/get-roles?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ROLES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}