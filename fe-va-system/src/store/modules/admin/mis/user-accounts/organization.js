import axios from '@/axios'

const MIS = 'mis';
export default {
    namespaced: true,
    state: {
        organization_accounts: {}
    },
    mutations: {
        SET_ORGANIZATION_ACCOUNTS: (state, organization_accounts) => state.organization_accounts = organization_accounts,
    },
    getters: {
        GET_ORGANIZATION_ACCOUNTS: (state) => state.organization_accounts,
    },
    actions: {
        FETCH_ORGANIZATION_ACCOUNTS: async ({ commit }) => {
            await axios.get(`${MIS}/organization-accounts?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ORGANIZATION_ACCOUNTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        CREATE_ORGANIZATION_ACCOUNT: async ({ commit }, data) => {
            const res = await axios.post(`${MIS}/organization-accounts?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        UPDATE_ORGANIZATION_ACCOUNT: async ({ commit }, data) => {
            const res = await axios.put(`${MIS}/organization-accounts/${data.id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        DELETE_ORGANIZATION_ACCOUNT: async ({ commit }, id) => {
            const res = await axios.delete(`${MIS}/organization-accounts/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        CHANGE_PASSWORD: async ({ commit }, data) => {
            const res = await axios.put(`${MIS}/organization-change-password/${data.id}?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        ACTIVATE_ORGANIZATION_ACCOUNT: async ({ commit }, id) => {
            const res = await axios.put(`${MIS}/organization-activate-account/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },

        DEACTIVATE_ORGANIZATION_ACCOUNT: async ({ commit }, id) => {
            const res = await axios.put(`${MIS}/organization-deactivate-account/${id}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },

        ACTIVATE_ORGANIZATION_ACCOUNTS: async ({ commit }) => {
            const res = await axios.put(`${MIS}/organization-activate-accounts?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },

        DEACTIVATE_ORGANIZATION_ACCOUNTS: async ({ commit }) => {
            const res = await axios.put(`${MIS}/organization-deactivate-accounts?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
    }
}