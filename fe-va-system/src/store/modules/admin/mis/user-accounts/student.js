import axios from '@/axios'

const MIS = 'mis';
export default {
    namespaced: true,
    state: {
        student_accounts: {}
    },
    mutations: {
        SET_STUDENT_ACCOUNTS: (state, student_accounts) => state.student_accounts = student_accounts,
    },
    getters: {
        GET_STUDENT_ACCOUNTS: (state) => state.student_accounts,
    },
    actions: {
        FETCH_STUDENT_ACCOUNTS: async ({ commit }) => {
            await axios.get(`${MIS}/student-accounts?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_STUDENT_ACCOUNTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        DEACTIVATE_STUDENT_ACCOUNTS: async ({ commit }) => {
            const res = await axios.put(`${MIS}/student-deactivate-accounts?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
    }
}