import axios from '@/axios'

const AUTH = "auth/admin"

export default {
    namespaced: true,
    state: {
        user: {},
        access_level: {},
        role: {},
        organization: {},
        token: localStorage.getItem('access_token') || '',
    },
    getters: {
        GET_ACCESS_LEVEL: (state) => state.access_level,
        GET_ROLE: (state) => state.role,
        GET_ADMIN_TOKEN: (state) => state.token,
        GET_ADMIN_USER: (state) => state.user,
        GET_ADMIN_ORGANIZATION: (state) => state.organization,

    },
    mutations: {
        SET_ACCESS_LEVEL: (state, access_level) => {
            state.access_level = access_level;
        },
        SET_ORGANIZATION: (state, organization) => {
            state.organization = organization;
        },
        SET_ROLE: (state, role) => {
            state.role = role;
        },
        SET_ADMIN_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            state.token = token;
            const bearer_token = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${bearer_token}`;
        },
        SET_ADMIN_USER: (state, user) => {
            let loginType = user.access_level_id == 1 ? 'isMIS' : 'isORG'
            localStorage.setItem(loginType, 'true');
            state.user = user;

        },
        UNSET_ADMIN_TOKEN: (state) => {
            localStorage.removeItem("access_token");
            let loginType = state.user.access_level_id == 1 ? 'isMIS' : 'isORG'
            localStorage.removeItem(loginType);
            state.token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },
        UNSET_ADMIN_USER: (state) => {
            state.user = {}
        },
    },
    actions: {
        LOGOUT: async ({ commit }) => {
            const res = await axios.post(`${AUTH}/logout?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("UNSET_ADMIN_TOKEN");
                    commit("UNSET_ADMIN_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        LOGIN: async ({ commit }, user) => {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    commit("SET_ACCESS_LEVEL", response.data.access_level);
                    commit("SET_ADMIN_TOKEN", response.data.access_token);
                    commit("SET_ADMIN_USER", response.data.user);
                    commit("SET_ROLE", response.data.role);
                    commit("SET_ORGANIZATION", response.data.organization);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        CHECK: async ({ commit }) => {
            const res = await axios.post(
                `${AUTH}/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_ACCESS_LEVEL", response.data.access_level);
                    commit("SET_ADMIN_USER", response.data.user);
                    commit("SET_ROLE", response.data.role);
                    commit("SET_ORGANIZATION", response.data.organization);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}