import axios from '@/axios'

export default {
    state: {
        colleges: {},
        courses: {},
        parties: {},
        positions: {},
        school_years: {},
        semeters: {},
        year_levels: {},
        election_id: '',
        toggle: false
    },
    mutations: {
        SET_COLLEGES: (state, colleges) => state.colleges = colleges,
        SET_COURSES: (state, courses) => state.courses = courses,
        SET_PARTIES: (state, parties) => state.parties = parties,
        SET_POSITIONS: (state, positions) => state.positions = positions,
        SET_SCHOOL_YEARS: (state, school_years) => state.school_years = school_years,
        SET_SEMESTERS: (state, semeters) => state.semeters = semeters,
        SET_YEAR_LEVELS: (state, year_levels) => state.year_levels = year_levels,
        SET_ELECTIONS: (state, elections) => state.elections = elections,
        SET_CANDIDATES(state, { data, id }) {
            state.candidates = data
            state.election_id = id
        },
        SET_TOGGLE: (state) => state.toggle = !state.toggle
    },
    getters: {
        GET_COLLEGES: (state) => state.colleges,
        GET_COURSES: (state) => state.courses,
        GET_PARTIES: (state) => state.parties,
        GET_POSITIONS: (state) => state.positions,
        GET_SCHOOL_YEARS: (state) => state.school_years,
        GET_SEMESTERS: (state) => state.semeters,
        GET_YEAR_LEVELS: (state) => state.year_levels,
        GET_CANDIDATES: (state) => state.candidates,
        GET_VOTE_COUNTS: (state) => state.vote_counts,
        GET_ELECTION_ID: (state) => state.election_id,
        GET_TOGGLE: (state) => state.toggle

    },
    actions: {
        async FETCH_PARTIES({ commit }) {
            await axios.get('parties')
                .then((response) => {
                    commit("SET_PARTIES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_POSITIONS({ commit }) {
            await axios.get('positions')
                .then((response) => {
                    commit("SET_POSITIONS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_COLLEGES({ commit }) {
            await axios.get('colleges')
                .then((response) => {
                    commit("SET_COLLEGES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_COURSES({ commit }) {
            await axios.get('courses')
                .then((response) => {
                    commit("SET_COURSES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_SEMESTERS({ commit }) {
            await axios.get('semesters')
                .then((response) => {
                    commit("SET_SEMESTERS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_YEAR_LEVELS({ commit }) {
            await axios.get('year_levels')
                .then((response) => {
                    commit("SET_YEAR_LEVELS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_SCHOOL_YEARS({ commit }) {
            await axios.get('school_years')
                .then((response) => {
                    commit("SET_SCHOOL_YEARS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}