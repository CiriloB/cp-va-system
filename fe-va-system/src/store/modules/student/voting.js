import axios from '@/axios/'

const STUDENT = 'student';

export default {
    namespaced: true,
    state: {
        candidates: {},
        elections: {},
        election_id: '',
    },
    mutations: {
        SET_ELECTIONS: (state, elections) => state.elections = elections,
        SET_CANDIDATES(state, { data, id }) {
            state.candidates = data
            state.election_id = id
        },
    },
    getters: {
        GET_CANDIDATES: (state) => state.candidates,
        GET_ELECTIONS: (state) => state.elections,
        GET_ELECTION_ID: (state) => state.election_id,

    },
    actions: {
        FETCH_ELECTIONS: async ({ commit }) => {
            await axios.get(`${STUDENT}/get-elections?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ELECTIONS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        FETCH_CANDIDATES: async ({ commit }, id) => {
            const res = await axios.get(`${STUDENT}/get-candidates?id=${id}&token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_CANDIDATES", { data: response.data, id: id });
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STORE_VOTE: async ({ commit }, data) => {
            const res = await axios.post(`${STUDENT}/submit-vote?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}