import axios from '@/axios'

const STUDENT = 'student'
export default {
    namespaced: true,
    state: {
        vote_counts: {},
        organizations: {},
        announcements: {}
    },
    mutations: {
        SET_VOTE_COUNTS: (state, vote_counts) => state.vote_counts = vote_counts,
        SET_ORGANIZATIONS: (state, organizations) => state.organizations = organizations,
        SET_LATEST_ANNOUNCEMENTS: (state, announcements) => state.announcements = announcements,
    },
    getters: {
        GET_VOTE_COUNTS: (state) => state.vote_counts,
        GET_ORGANIZATIONS: (state) => state.organizations,
        GET_LATEST_ANNOUNCEMENTS: (state) => state.announcements,
    },
    actions: {
        async FETCH_VOTE_COUNT({ commit }, organization_id) {
            const res = await axios.get(`${STUDENT}/vote-count?organization_id=${organization_id}&token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_VOTE_COUNTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        FETCH_ORGANIZATIONS: async ({ commit }) => {
            const res = await axios.get(`${STUDENT}/get-organizations?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ORGANIZATIONS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        FETCH_LATEST_ANNOUNCEMENTS: async ({ commit }) => {
            const res = await axios.get(`${STUDENT}/get-latest-announcements?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_LATEST_ANNOUNCEMENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}