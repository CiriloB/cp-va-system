import axios from '@/axios'

const STUDENT = 'student'
export default {
    namespaced: true,
    state: {
        announcements: {},
        announcement: {},
    },
    mutations: {
        SET_ANNOUNCEMENTS: (state, announcements) => state.announcements = announcements,
        SET_ANNOUNCEMENT: (state, announcement) => state.announcement = announcement,
    },
    getters: {
        GET_ANNOUNCEMENTS: (state) => state.announcements,
        GET_ANNOUNCEMENT: (state) => state.announcement,
    },
    actions: {
        FETCH_ANNOUNCEMENTS: async ({ commit }, organization_id) => {
            await axios.get(`${STUDENT}/get-announcements?organization_id=${organization_id}&token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ANNOUNCEMENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },

        SINGLE_ANNOUNCEMENT: async ({ commit }, slug) => {
            await axios.get(`${STUDENT}/single-announcement-student/${slug}?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("SET_ANNOUNCEMENT", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}