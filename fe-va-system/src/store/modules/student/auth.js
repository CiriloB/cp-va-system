import axios from '@/axios'

const AUTH = "auth/student"

export default {
    namespaced: true,
    state: {
        user: {},
        token: localStorage.getItem('access_token') || ''
    },
    getters: {
        GET_TOKEN: (state) => state.token,
        GET_USER: (state) => state.user
    },
    mutations: {
        SET_USER: (state, user) => {
            state.user = user;

        },
        SET_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            localStorage.setItem('isUser', 'true');
            state.token = token;
            const bearer_token = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${bearer_token}`;
        },
        UNSET_USER: (state) => {
            localStorage.removeItem("access_token");
            localStorage.removeItem("isUser");
            state.token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },
    },
    actions: {
        LOGOUT: async ({ commit }) => {
            const res = await axios.post(`${AUTH}/logout?token=${localStorage.getItem('access_token')}`)
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        LOGIN: async ({ commit }, user) => {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    if (response.data.student_status) {
                        return response;
                    } else {
                        commit("SET_USER", response.data.user);
                        commit("SET_TOKEN", response.data.access_token);
                        return response;

                    }
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        CHECK: async ({ commit }) => {
            const res = await axios.post(
                `${AUTH}/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_USER", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        SIGNUP: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/store`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        UPDATE_ACCOUNT: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/update-account?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        UPDATE_ACCOUNT_INFO: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/update-account-info?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        RESET_PASSWORD_REQUEST: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/reset-password-request?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        CHANGE_PASSWORD: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/change-password?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        EMAIL_VERIFICATION_REQUEST: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/email-verification-request?token=${localStorage.getItem('access_token')}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}