import Vue from 'vue'
import Vuex from 'vuex'
import authAdmin from './modules/admin/auth'
import authStudent from './modules/student/auth';
// Org admin
import adminOrgDashboard from './modules/admin/organization/dashboard'
import adminOrgAnnouncement from './modules/admin/organization/announcement'
import adminOrgStudentAccount from './modules/admin/organization/student_account'
import adminOrgElection from './modules/admin/organization/election'
// Student
import studentDashboard from './modules/student/dashboard';
import studentVoting from './modules/student/voting';
import studentAnnouncement from './modules/student/announcement';
// MIS Admin
import misDashboard from './modules/admin/mis/dashboard'
import misStudentAccount from './modules/admin/mis/user-accounts/student'
import misOrganizationAccount from './modules/admin/mis/user-accounts/organization'


import utils from './modules/utils'



Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        authAdmin,
        authStudent,
        misDashboard,
        misStudentAccount,
        misOrganizationAccount,
        adminOrgDashboard,
        adminOrgAnnouncement,
        adminOrgStudentAccount,
        adminOrgElection,
        studentDashboard,
        studentVoting,
        studentAnnouncement,
        utils
    }
});