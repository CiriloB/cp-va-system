import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import titleMixin from './mixin/title';
import Toast from "vue-toastification";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.config.productionTip = false

import 'swiper/css/swiper.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";
import '@/assets/css/main.css'

const options = {
  // You can set your default options here
};


Vue.mixin(titleMixin);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Toast, options);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
